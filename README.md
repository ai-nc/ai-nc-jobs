# AI-NC Coding Challenge
![Version 1](https://img.shields.io/badge/Parallell%20Problems-v1.0.0%20-ff69b4.svg)

![vice](https://i.vimeocdn.com/video/29470888-fd6af896425cbdac48de2753a5397a3ab8721e8d24ca589e956e2f8896164f4c-d_640)

## Problem Statement

Most parts made in CNC milling machines are held in a vice during machining. A challenge we face when trying to
automate the programming of these machines is to figure out the optimal orientation the part is held (if it can be at all!).

Your task is to write a program that:

1. Reads an IGES file (3D model) of a single part.
2. Finds the set of all parallel face pairs which are _entirely_ unobstructed from the outside.
3. Returns a list of face pairs, ordered in descending order of combined surface area.

Feel free to use any programming language you're comfortable with, and if you have any questions let us know and we'll
help clarify as best we can.

You are provided with two test parts in ```data/``` to use to test your program, when we test your program we will use the same parts, 
as well as some more challenging ones. Details on the IGES file format can be found [here](https://wiki.eclipse.org/IGES_file_Specification)
if you decide to write your own code to parse the file, otherwise feel fee to use libraries you might find (e.g. [pyiges](https://pyvista.github.io/pyiges/index.html)).

Your code will be 'graded' based on:

- Efficiency
- Accuracy
- Stability
- Coding style & overall quality
- Documentation

